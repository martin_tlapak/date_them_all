Welcome to date-them-all's documentation!
=========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   project_description
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
